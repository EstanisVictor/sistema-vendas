package salesSystem.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import salesSystem.demo.models.Product;
import salesSystem.demo.models.Stock;
import salesSystem.demo.repositories.ProductRepo;
import salesSystem.demo.repositories.StockRepo;

@Service
public class StockService {
    
    private final StockRepo stockRepo;
    private final ProductRepo productRepo;

    @Autowired
    public StockService(StockRepo stockRepo, ProductRepo productRepo){
        this.stockRepo = stockRepo;
        this.productRepo = productRepo;
    }

    @Transactional
    public Stock addStock(Stock stock){
        return stockRepo.save(stock);
    }

    @Transactional
    public void addProductStock(Long productId, Long stockId){
        Product product = productRepo.findById(productId).orElseThrow();
        Stock stock = stockRepo.findById(stockId).orElseThrow();
        stock.getProducts().add(product);
    }

    public List<Stock> findAllProductStock(){
        return stockRepo.findAll();
    }

}
