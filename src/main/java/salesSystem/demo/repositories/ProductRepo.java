package salesSystem.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import salesSystem.demo.models.Product;

public interface ProductRepo extends JpaRepository<Product, Long> {
    
}
