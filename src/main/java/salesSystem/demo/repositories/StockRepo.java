package salesSystem.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import salesSystem.demo.models.Stock;

public interface StockRepo extends JpaRepository<Stock, Long>{
    
}
