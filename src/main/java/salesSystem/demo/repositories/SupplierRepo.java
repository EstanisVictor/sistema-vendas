package salesSystem.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import salesSystem.demo.models.Supplier;

public interface SupplierRepo extends JpaRepository<Supplier, Long> {
    
}
