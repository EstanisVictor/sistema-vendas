package salesSystem.demo.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.Data;
import salesSystem.demo.models.Stock;
import salesSystem.demo.service.StockService;

@RestController
@RequestMapping("/stock")
public class StockController {

    private StockService stockService;

    public StockController(StockService stockService) {
        this.stockService = stockService;
    }

    @PostMapping("/addProductStock")
    public ResponseEntity<?> addProductStock(@RequestBody ProductStock productStock) {
        stockService.addProductStock(productStock.getProductId(), productStock.getStockId());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<?> addStock(@RequestBody Stock stock) {
        Stock stockAdded = stockService.addStock(stock);
        return new ResponseEntity<>(stockAdded, HttpStatus.OK);
    }

    @GetMapping("/all")
    public ResponseEntity<List<Stock>> getProductAll() {
        List<Stock> products = stockService.findAllProductStock();
        return new ResponseEntity<>(products, HttpStatus.OK);
    }
}

@Data
class ProductStock {
    private Long productId;
    private Long stockId;
}